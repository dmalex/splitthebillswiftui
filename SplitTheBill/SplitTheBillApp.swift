//
//  SplitTheBillApp.swift
//  SplitTheBill
//  Created by brfsu on 21.01.2022.
//
import SwiftUI

@main
struct SplitTheBillApp: App
{
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
